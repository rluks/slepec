package com.me.mygdxgame;

/**
 *   Prototyp hry vyvinut� na Geeewa Hackathon 2013
 *   T�m: Roman Luk�, Luk� Hlavat�
 */

import java.awt.Point;
import java.awt.Rectangle;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public class MyGdxGame implements ApplicationListener {
	private OrthographicCamera camera;
	private SpriteBatch spriteBatch;
	
	Texture PlayerImage0;
	Texture PlayerImage1;
	Texture PlayerImage0d;
	Texture PlayerImage1d;
	Texture PlayerStand;
	Rectangle player;
	
	Texture prekryv0;
	Texture prekryv0d;
	
	Texture backgroundImage;
	
	Music loopMusic;

	
	private static final int PREKRYV_X = 816-32;
	private static final int PREKRYV_Y = 496-32;
	private static final int BG_HEIGHT = 7;
	
	Texture test;
	
	TextureAtlas myTextures;

	private static final int PLAYER_SIZE = 64;
	private static final int MOVEMENT_SPEED = 70;
	private static final int PLAYER_ANIM_DELAY = 5;
	
	private static final boolean KROK0 = false;
	private static final boolean KROK1 = true;
	
	private Point start;
	
	private Vector2 player_pos;
	
	private boolean animKroku;
	private int zpozdeni;
	
	
	private Vector2 objectDirection;		
	int screenWidth, screenHeight;
	
	@Override
	public void create() {				
		
		start = new Point();
		start.x = 0;
		start.y = 50;
		
		player = new Rectangle();
		player.x = start.x;
		player.y = start.y;
		player.width = PLAYER_SIZE;
		player.height = PLAYER_SIZE;
		
		zpozdeni = PLAYER_ANIM_DELAY;
		
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		
	    camera = new OrthographicCamera();
	    camera.setToOrtho(false, 800, 480);
	   
	    spriteBatch = new SpriteBatch();
		
		PlayerImage0 = new Texture(Gdx.files.internal("player0.png"));
		PlayerImage1 = new Texture(Gdx.files.internal("player1.png"));
		PlayerImage0d = new Texture(Gdx.files.internal("player0d.png"));
		PlayerImage1d = new Texture(Gdx.files.internal("player1d.png"));
		PlayerStand = new Texture(Gdx.files.internal("player-stand.png"));
		
		prekryv0 = new Texture(Gdx.files.internal("p�ekryv0.png"));
		prekryv0d = new Texture(Gdx.files.internal("p�ekryv0d.png"));
		
		backgroundImage = new Texture(Gdx.files.internal("bg.png"));		
		
		loopMusic = Gdx.audio.newMusic(Gdx.files.internal("background sound SMY�KA vol3.mp3"));
		
		loopMusic.setLooping(true);
		loopMusic.play();
	
		player_pos = new Vector2(start.x, start.y);
		
		objectDirection = new Vector2(1, 0);


	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
		PlayerImage0.dispose();
		PlayerImage1.dispose();
		PlayerStand.dispose();
		
	}

	@Override
	public void render() {	
		
		update();

		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		
		spriteBatch.draw(backgroundImage, 0, 0);
		
		// Draw object
		if(objectDirection.x != 0){
			if(this.animKroku){
				if(objectDirection.x > 0){//pravo
					spriteBatch.draw(PlayerImage0, player.x, player.y);
					spriteBatch.draw(prekryv0, player.x-PREKRYV_X, player.y-PREKRYV_Y);
					
				}else{
				spriteBatch.draw(PlayerImage0d, player.x, player.y);//doleva
				spriteBatch.draw(prekryv0d, player.x-PREKRYV_X, player.y-PREKRYV_Y);
				}
			}else{
				if(objectDirection.x > 0){//pravo
					spriteBatch.draw(PlayerImage1, player.x, player.y);
					spriteBatch.draw(prekryv0, player.x-PREKRYV_X, player.y-PREKRYV_Y);
				}else{
				spriteBatch.draw(PlayerImage1d, player.x, player.y);//doleva
				spriteBatch.draw(prekryv0d, player.x-PREKRYV_X, player.y-PREKRYV_Y);
				}
			}
		}else{
			spriteBatch.draw(PlayerStand, player.x, player.y);
			spriteBatch.draw(prekryv0d, player.x-PREKRYV_X-10, player.y-PREKRYV_Y);
		}
		
		
		spriteBatch.end();
	}
	
	private void update() {
		Vector2 direction = new Vector2(0, 0);		
		
		zpozdeni--;
		if(zpozdeni == 0){
			this.animKroku = (this.animKroku) ? KROK0 : KROK1;
			zpozdeni = PLAYER_ANIM_DELAY;
		}
		
		float delta = Gdx.graphics.getDeltaTime() * MOVEMENT_SPEED;
		if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT)) {
			direction.x = 1 * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT)) {
			direction.x = -1 * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DPAD_UP)) {
			direction.y = 1 * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DPAD_DOWN)) {
			direction.y = -1 * delta;
		}
		
		//gravitace
		direction.y -= 1.5;
		
		if (direction.x != 0 || direction.y != 0) {
			player_pos.add(direction);
			if (player_pos.x < 0){
				player_pos.x = 0;
			}
			if (player_pos.x > this.screenWidth - PLAYER_SIZE){
				player_pos.x = this.screenWidth - PLAYER_SIZE;
			}
			if (player_pos.y < BG_HEIGHT){
				player_pos.y = BG_HEIGHT;
			}
			if (player_pos.y > this.screenHeight - PLAYER_SIZE){
				player_pos.y = this.screenHeight - PLAYER_SIZE;
			}
			objectDirection.set(direction);
		}
		
		player.x = (int)player_pos.x;
		player.y = (int)player_pos.y;	
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
